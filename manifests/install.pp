class javas::install {
  file { '/etc/profile.d/java.sh':
    ensure  => 'file',
    content => epp('javas/java.sh.epp'),
  }

  file { $javas::distributions_dir:
    ensure => 'directory',
  }

  $javas::distributions.each |$name, $distribution| {
    $jce_url = $distribution['jce'] ? {
      true    => $javas::jce_urls[$distribution['version']],
      default => undef,
    }

    javas::download_java { $name:
      type     => $distribution['type'],
      version  => $distribution['version'],
      update   => $distribution['update'],
      build    => $distribution['build'],
      hash     => $distribution['hash'],
      platform => $distribution['platform'],
      jce_url  => $jce_url,
      require  => File[$javas::distributions_dir],
    }
  }

  file { "${javas::distributions_dir}/current":
    ensure  => 'link',
    target  => "${javas::distributions_dir}/${javas::main_distribution}",
    require => Javas::Download_java[$javas::main_distribution],
  }
}
