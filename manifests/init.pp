class javas (
  String $main_distribution               = $javas::params::main_distribution,
  String $download_url                    = $javas::params::download_url,
  Stdlib::Absolutepath $distributions_dir = $javas::params::distributions_dir,
  Hash[String, Struct[{
    version => String,
    update => String,
    build => String,
    type => Enum['jre', 'jdk'],
    Optional[hash] => String,
    Optional[jce] => Boolean
  }]]
  $distributions                          = $javas::params::distributions,
  String $default_platform                = $javas::params::default_platform,
  Hash[String, String] $jce_urls          = $javas::params::jce_urls,
) inherits javas::params {
  if ! has_key($distributions, $main_distribution) {
    fail('The main distribution isn\'t amongst the listed distributions')
  }

  class{'javas::install': }
  -> Class['javas']
}
