define javas::download_java(
  Enum['jre', 'jdk'] $type,
  String $version,
  String $update,
  String $build,
  String $hash,
  String $platform                        = $javas::default_platform,
  String $download_url                    = $javas::download_url,
  Stdlib::Absolutepath $distributions_dir = $javas::distributions_dir,
  Optional[String] $jce_url               = undef,
) {
  $replacements = {
    '\{type\}'     => $type,
    '\{version\}'  => $version,
    '\{update\}'   => $update,
    '\{build\}'    => $build,
    '\{hash\}'     => $hash,
    '\{platform\}' => $platform,
  }
  $url = $replacements.reduce($download_url) | $url, $replacement | {
    regsubst($url, $replacement[0], $replacement[1], 'G')
  }

  wget::fetch { "Download ${name}":
    source      => $url,
    destination => "${distributions_dir}/${name}.tar.gz",
    no_cookies  => true,
    headers     => ['Cookie: oraclelicense=accept-securebackup-cookie'],
  }
  -> file { "${distributions_dir}/${name}":
    ensure => 'directory',
  }
  -> exec { "Extract ${name}":
    command => "tar -C ${name} --strip 1 -xzf ${name}.tar.gz",
    cwd     => $distributions_dir,
    path    => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/opt/local/bin:/usr/sfw/bin',
  }
  -> file { "${distributions_dir}/${name}.tar.gz":
    ensure => absent,
  }

  unless $jce_url == undef {
    ensure_packages('unzip')

    wget::fetch { "Download ${name} JCE":
      source      => $jce_url,
      destination => "${distributions_dir}/${name}_jce.zip",
      no_cookies  => true,
      headers     => ['Cookie: oraclelicense=accept-securebackup-cookie'],
    }
    -> exec { "Extract ${name} JCE":
      command => "unzip -o -j ${name}_jce.zip -d ${distributions_dir}/${name}/jre/lib/security/",
      cwd     => $distributions_dir,
      path    => '/usr/bin:/usr/sbin:/bin:/usr/local/bin:/opt/local/bin:/usr/sfw/bin',
      require => [Package['unzip'], Exec["Extract ${name}"]],
    }
    -> file { "${distributions_dir}/${name}_jce.zip":
      ensure => absent,
    }
  }
}
